import "reflect-metadata";
import App from './App';
import { UserController } from './controller/UserController';
import { createConnection } from 'typeorm';


const init = async () => {
  const connection = await createConnection();
  await connection.synchronize();

  const controllers = [
   new UserController()
  ];
  const app = new App(controllers, 4000);
  await app.init();
  app.listen();
};

init();
