import * as express from 'express';
import { createClient } from 'redis';
import { promisify } from 'util';
import { NextFunction, Request, Response } from 'express';

class App {
  public app: express.Application;
  public port: number;
  private controllers: any[];

  constructor (controllers: any[], port: number) {
    this.app = express();
    this.port = port;
    this.controllers = controllers;
  }

  init () {
    this.initializeMiddlewares();
    this.initializeControllers();
  }

  private initializeMiddlewares() {
    this.app.use(express.json());
    this.app.use(this.redisMiddleware)
  }

  public redisMiddleware(req: Request, res: Response, next: NextFunction) {
    const redis = createClient(6379, '127.0.0.1');
    const get = promisify(redis.get).bind(redis);
    const set = promisify(redis.set).bind(redis);
    const del = promisify(redis.del).bind(redis);
    req.redis = {
      get, set, del
    };
    next();
  }

  private initializeControllers() {
    this.controllers.forEach((controller) => {
      this.app.use('/', controller.router);
    });
  }

  public listen() {
    this.app.listen(this.port, () => {
      console.log(`Server running on port ${this.port}`);
    })
  }
}

export default App;
