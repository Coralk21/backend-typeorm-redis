import { getConnection } from 'typeorm';
import {Router, NextFunction, Request, Response} from "express";
import {User} from "../entity/User";


export class UserController {
  public path:string = '/user';
  public router: Router = Router();

  constructor () {
    this.initializeMiddlewares();
    this.initializeRoutes();
  }

  private initializeMiddlewares() {
    this.router.use(this.validateInput);
  }


  public initializeRoutes() {
    this.router.post(this.path, this.createUser);
    this.router.get(this.path, this.getAllUsers);
    this.router.get(`${this.path}/:id`, this.getUser);
    this.router.put(`${this.path}/:id`, this.updateUser);
    this.router.delete(`${this.path}/:id`, this.deleteUser);
  }

  public validateInput(req: Request, res: Response, next: NextFunction) {
    const params = {id: req.url.split('/')[2]};
    switch ( req.method ) {
      case 'GET':
        break;
      case 'DELETE':
        if(!params.id) { return res.status(400).send({message: 'Id is required'});}
        break;
      case 'POST':
        if(Object.keys(req.body).length === 0) { return res.status(400).send('Request body cannot be empty');}
        break;
      case 'PUT':
        if(!params.id) {return res.status(400).send({message: 'Id is required'});}
        if(Object.keys(req.body).length === 0){ return res.status(400).send({message:'Request body cannot be empty'});}
        break;
    }
    next();
  }

  public async createUser(req: Request, res: Response) {
    const userData = req.body;
    const user = new User();
    user.name = userData.name;
    user.email = userData.email;
    await getConnection().getRepository(User).save(user);
    await req.redis.set( `USER-${user.id}`, JSON.stringify(user) );
    await req.redis.del( `ALL_USERS` );
    return res.send(user);
  }

  public async getAllUsers(req: Request, res: Response) {
    let users = await req.redis.get( 'ALL_USERS' );
    if ( !users ) {
      users = await getConnection().getRepository(User).find();
      await req.redis.set( 'ALL_USERS', JSON.stringify(users) );
    }
    return res.set('Content-Type', 'application/json').send(users);
  }

  public async getUser(req: Request, res: Response) {
    const id = req.params.id;
    let user = await req.redis.get( `USER-${id}` );
    if ( !user ) {
      user = await getConnection().getRepository(User).findOne(id);
      if ( user ) {
        await user.redis.set( `USER-${id}`, JSON.stringify( user ) );
      }
    }
    if (!user) {
      return res.status(404).send({message: 'Not found'});
    }
    return res.set('Content-Type', 'application/json').send(user);
  }

  public async updateUser(req: Request, res: Response) {
    const id = req.params.id;
    const user = await getConnection().getRepository(User).findOne(id);
    if(user !== undefined) {
      await getConnection().getRepository(User).update(id, req.body);
      await req.redis.del( `ALL_USERS` );
      await req.redis.del(`USER-${id}`);
      return res.status(200).send({message: 'User updated correctly'});
    }
    return res.status(404).send({message: 'User not found'});
  }

  public async deleteUser(req: Request, res: Response) {
    const id = req.params.id;
    await getConnection().getRepository(User).delete(id);
    await req.redis.del(`USER-${id}`);
    await req.redis.del( `ALL_USERS` );
    return res.status(200).send({message: 'User deleted successfully'});
  }
}
